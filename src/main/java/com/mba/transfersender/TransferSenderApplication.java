package com.mba.transfersender;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TransferSenderApplication {

	public static void main(String[] args) {
		SpringApplication.run(TransferSenderApplication.class, args);
	}

}
