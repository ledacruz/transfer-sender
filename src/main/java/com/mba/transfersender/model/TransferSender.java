package com.mba.transfersender.model;


import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder

public class TransferSender {

    public String eventType;
    public String timestamp;
}
