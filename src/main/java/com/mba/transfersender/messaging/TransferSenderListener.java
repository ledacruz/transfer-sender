package com.mba.transfersender.messaging;

import com.google.gson.Gson;
import com.mba.transfersender.model.TransferSender;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class TransferSenderListener {

    @Autowired
    TransferSenderProducer transferSenderProducer;

    @Value("${topic-choreography}")
    private String choreographyTopic;

    @Value("${topic-out}")
    private String orchestratorTopic;

    private final Logger logger = LoggerFactory.getLogger(TransferSenderListener.class);

    private String producerMessage;

    @KafkaListener(topics = "${topic-choreography}", groupId = "group-transfer-sender")
    public void consumeChoreography(String message){
        logger.info(String.format("$$ -> Consumed Message -> %s",message));

        TransferSender transferSender = convertStringtoTransferSender(message);

        switch (transferSender.getEventType()) {
            case "fraud.sucess":
                producerMessage = "transferSender.sucess";
               // producerMessage = "transferSender.failure";
                callProducer(producerMessage, choreographyTopic);
                break;
            case "receipts.failure":
                producerMessage = "transferSender.undo.sucess";
                //producerMessage = "transferSender.undo.failure";
                callProducer(producerMessage, choreographyTopic);
                break;
            case "fraud.failure":
                producerMessage = "transferSender.undo.sucess";
                //producerMessage = "transferSender.undo.failure";
                callProducer(producerMessage, choreographyTopic);
                break;
            default:
                logger.info(String.format("$$ -> Consumed not a know Message -> %s",message));
        }

    }

    @KafkaListener(topics = "${topic-in}", groupId = "group-transfer-sender")
    public void consumeOrchestration(String message){
        logger.info(String.format("$$ -> Consumed Message -> %s",message));

        TransferSender transferSender = convertStringtoTransferSender(message);

        switch (transferSender.getEventType()) {
            case "transfer.sender.requested":
                producerMessage = "transferSender.sucess";
              //  producerMessage = "transferSender.failure";
                callProducer(producerMessage, orchestratorTopic);
                break;
            case "undo.requested":
                producerMessage = "transferSender.undo.sucess";
                //producerMessage = "transferSender.undo.failure";
                callProducer(producerMessage, orchestratorTopic);
                break;
        }
    }

    private void callProducer(String message, String topic){

        transferSenderProducer.sendMessage(producerMessage, topic);
    }

    private TransferSender convertStringtoTransferSender(String message){
        Gson gson = new Gson();
        TransferSender transferSender = gson.fromJson(message, TransferSender.class);

        return transferSender;

    }


}
