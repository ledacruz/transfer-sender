package com.mba.transfersender.messaging;

import com.google.gson.Gson;
import com.mba.transfersender.model.TransferSender;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j

@Service
public class TransferSenderProducer {
    private static final Logger logger = LoggerFactory.getLogger(TransferSenderProducer.class);

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    public void sendMessage(String message, String topic){

        String record = convertTransferSenderToString(message);

        logger.info(String.format("$$ -> Producing message --> %s", record));
        this.kafkaTemplate.send(topic, record);
    }

    private String getDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy-HH:mm:ss.SSS");
        Date date = new Date();
        return dateFormat.format(date);
    }


    private String convertTransferSenderToString(String message){

        Gson gson = new Gson();
        TransferSender transferSender = TransferSender.builder().eventType(message).timestamp(getDateTime()).build();;
        String finalMessage = gson.toJson(transferSender);

        return finalMessage;

    }
}