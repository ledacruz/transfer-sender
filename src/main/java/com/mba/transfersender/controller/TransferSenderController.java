package com.mba.transfersender.controller;

import com.mba.transfersender.messaging.TransferSenderProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

    @RestController
    @RequestMapping(value = "/kafka")
    public class TransferSenderController {
        private final TransferSenderProducer transferSenderProducer;

        @Autowired
        public TransferSenderController(TransferSenderProducer transferSenderProducer) {
            this.transferSenderProducer = transferSenderProducer;
        }
        @PostMapping(value = "/publish")
        public void sendMessageToKafkaTopic(@RequestParam("message") String message){
            this.transferSenderProducer.sendMessage(message, "");
        }
    }